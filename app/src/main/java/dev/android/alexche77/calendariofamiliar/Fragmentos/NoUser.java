package dev.android.alexche77.calendariofamiliar.Fragmentos;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import dev.android.alexche77.calendariofamiliar.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NoUser.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NoUser#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NoUser extends Fragment implements View.OnClickListener{

    private OnFragmentInteractionListener mListener;

    public NoUser() {
        // Required empty public constructor
    }


    public static NoUser newInstance() {
        return new NoUser();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_no_user, container, false);
        Button btn = v.findViewById(R.id.btn1);
        Button btn2 = v.findViewById(R.id.btn2);
        btn.setOnClickListener(this);
        btn2.setOnClickListener(this);

//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (mListener != null) {
//                    Log.d("LOGIN","Iniciando la acitity de login");
//                    mListener.Login();
//                }
//            }
//        });
        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        mListener.Tocar(view);
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void Login();
        void Tocar(View view);
    }

}
