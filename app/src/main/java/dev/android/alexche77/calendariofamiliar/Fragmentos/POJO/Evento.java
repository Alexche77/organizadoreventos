package dev.android.alexche77.calendariofamiliar.Fragmentos.POJO;


public class Evento {


/*NOTA:
* En este POJO no se contempla el URL de la imagen puesto que se va a obtener desde firebase
* haciendo uso del ID del evento.
*
* Ejemplo:
*       eventos/miniatura/<id_evento>
*           Carga la imagen miniatura del evento.
*       eventos/portada/<id_evento>
*           Carga la imagen grande del evento.
* */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId_detalle() {
        return id_detalle;
    }

    public void setId_detalle(String id_detalle) {
        this.id_detalle = id_detalle;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAnho() {
        return anho;
    }

    public void setAnho(int anho) {
        this.anho = anho;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    private String id;
    private String nombre;
    private String id_detalle;
    private int dia, mes, anho;
    private long timestamp;


    public Evento(String id, String nombre, String id_detalle, int dia, int mes, int anho, long timestamp) {
        this.id = id;
        this.nombre = nombre;
        this.id_detalle = id_detalle;
        this.dia = dia;
        this.mes = mes;
        this.anho = anho;
        this.timestamp = timestamp;
    }


        @Override
        public String toString() {
            return "Evento: "+nombre+" para el: "+dia+" de "+mes+" del "+anho;
        }

    }

