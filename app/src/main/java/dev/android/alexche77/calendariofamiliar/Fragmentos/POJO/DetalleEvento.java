package dev.android.alexche77.calendariofamiliar.Fragmentos.POJO;

/**
 * Created by alvaro on 9/15/17.
 */

public class DetalleEvento {

//    Debe ser enviada al nodo del evento que pertenece, para que se llame al momento
//    que se da clic en el evento deseado para ver sus detalles.
    private String idCreador;           //Id del creador del evento (Usuario)
    private String listaInvitados;      //Id del nodo de lista de invitados correspondiente.
    private int invitados;              //Total de personas invitadas al evento.
    private int asistiran;              //Total de personas que asistiran al evento.
    private int noAsistiran;            //Total de personas que no asistiran al evento.
    private int pendientes;             //Total de personas que no han respondido.
    private String idPosts;             //Id del nodo de posts pertencientes a este evento.

    public String getIdCreador() {
        return idCreador;
    }

    public void setIdCreador(String idCreador) {
        this.idCreador = idCreador;
    }

    public String getListaInvitados() {
        return listaInvitados;
    }

    public void setListaInvitados(String listaInvitados) {
        this.listaInvitados = listaInvitados;
    }

    public int getInvitados() {
        return invitados;
    }

    public void setInvitados(int invitados) {
        this.invitados = invitados;
    }

    public int getAsistiran() {
        return asistiran;
    }

    public void setAsistiran(int asistiran) {
        this.asistiran = asistiran;
    }

    public int getNoAsistiran() {
        return noAsistiran;
    }

    public void setNoAsistiran(int noAsistiran) {
        this.noAsistiran = noAsistiran;
    }

    public int getPendientes() {
        return pendientes;
    }

    public void setPendientes(int pendientes) {
        this.pendientes = pendientes;
    }

    public String getIdPosts() {
        return idPosts;
    }

    public void setIdPosts(String idPosts) {
        this.idPosts = idPosts;
    }
/*NOTA:
* En este POJO no se contempla el URL de la imagen puesto que se va a obtener desde firebase
* haciendo uso del ID del evento.
*
* Ejemplo:
*       eventos/miniatura/<id_evento>
*           Carga la imagen miniatura del evento.
*       eventos/portada/<id_evento>
*           Carga la imagen grande del evento.
* */

}
