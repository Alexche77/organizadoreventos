package dev.android.alexche77.calendariofamiliar.Fragmentos.POJO;

/**
 * Created by alvaro on 9/15/17.
 *
 * Esta entidad sera utilizada para el momento que se vaya a invitar a un usuario a un evento
 * creado.
 *
 *
 * La invitacion se realizara mediante el correo o el ID unico que tenga cada usuario, visible en el
 * perfil de cada usuario y que podra compartir a como desee con el organizador del evento.
 *
 *
 * El ID de cada usuario es AUTOGENERADO por firebase.
 *
 */

public class Usuario {


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    //    Esto es lo unico que necesito saber del usuario al que estoy invitando.
    private String nombre;
    private String correo;



}
