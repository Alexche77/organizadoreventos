package dev.android.alexche77.calendariofamiliar.Fragmentos;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import dev.android.alexche77.calendariofamiliar.CrearEvento;
import dev.android.alexche77.calendariofamiliar.MisPortadasAdapter;
import dev.android.alexche77.calendariofamiliar.R;

/**
 * Created by alvaro on 9/23/17.
 */

public class DialogFragmentPortadas extends DialogFragment implements MisPortadasAdapter.ListenerPortada{
    ArrayList<StorageReference> referenciasPortadas;
    MisPortadasAdapter misPortadasAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.picker_portadas, container, false);
        RecyclerView recyclerView = v.findViewById(R.id.portadas);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//        Arreglo de referencias de almacenamiento
        referenciasPortadas = new ArrayList<>();
        Log.d("PORTADAS","Creando el DialogFragment");
        //        Obtenemos la referencia de almacenamiento de nuestro proyecto
        StorageReference ref = FirebaseStorage.getInstance().getReference();
//        Nos movemos a la carpeta deseada (HIJO)
        ref.child("nuevacarpeta");
        Log.d("PORTADAS", "Referencia" + ref.getPath());

//        for (int i = 1; i <= 3; i++) {
//            Log.d("PORTADAS","Obteniendo "+ref.child(i + ".png").getPath());
//            referenciasPortadas.add(ref.child(i + ".png"));
//        }
//        Aqui le indicamos el archivo a buscar dentro de la ruta
//        nuevacarpeta/<nombrearchivo>
        referenciasPortadas.add(ref.child("vid.webm"));
        if (referenciasPortadas != null){
            misPortadasAdapter = new MisPortadasAdapter(getContext(),referenciasPortadas);
            recyclerView.setAdapter(misPortadasAdapter);
            misPortadasAdapter.setClickListener(this);
        }




        return v;
    }

    @Override
    public void onItemClick(StorageReference reference) {
        CrearEvento activity = (CrearEvento)getActivity();
        activity.referenciaSeleccionada(reference);
    }

}
