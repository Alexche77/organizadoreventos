package dev.android.alexche77.calendariofamiliar.Fragmentos;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;

import dev.android.alexche77.calendariofamiliar.R;

public class Perfil extends Fragment {


    private ListenerFragment mListener;

    public Perfil() {
        // Required empty public constructor
    }
    public static Perfil newInstance() {


        return new Perfil();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_perfil, container, false);

        TextView nombre = v.findViewById(R.id.nombre);
        TextView id = v.findViewById(R.id.userId);
        nombre.setText(FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
        String cad = "Comparte este ID: \n"+FirebaseAuth.getInstance().getCurrentUser().getUid();
        id.setText(cad);
        ImageView imageView = v.findViewById(R.id.imagenUser);
        Glide.with(this).load(FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl()).into(imageView);
        Button btnMisEventos = v.findViewById(R.id.btnMisEventos);
        btnMisEventos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.ListenerMisEventos();
                }
            }
        });

        Button btnLogOut = v.findViewById(R.id.btnLogOut);
        btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.CerrarSesion();
                }
            }
        });
        return v;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ListenerFragment) {
            mListener = (ListenerFragment) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface ListenerFragment {
        void ListenerMisEventos();
        void CerrarSesion();
    }
}
