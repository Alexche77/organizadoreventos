package dev.android.alexche77.calendariofamiliar;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Arrays;

import dev.android.alexche77.calendariofamiliar.Fragmentos.EventoFragment;
import dev.android.alexche77.calendariofamiliar.Fragmentos.NoUser;
import dev.android.alexche77.calendariofamiliar.Fragmentos.POJO.Evento;
import dev.android.alexche77.calendariofamiliar.Fragmentos.Perfil;

public class MainActivity extends AppCompatActivity implements Perfil.ListenerFragment, EventoFragment.OnListFragmentInteractionListener, NoUser.OnFragmentInteractionListener{

    private static final int SIGN_IN_CODE = 200;
    private FloatingActionButton fab;
    private FirebaseAuth.AuthStateListener authStateListener;
    private FirebaseAuth firebaseAuth;
    private SectionsPagerAdapter mSectionsPagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        ViewPager mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setSelectedTabIndicatorHeight(15);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0)
                    fab.show(); else fab.hide();

            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        firebaseAuth = FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                actualizarInterfaz();
            }
        };


        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,CrearEvento.class);
                startActivity(intent);
            }
        });


    }

//
//
//    private void iniciarLoginActivity() {
//        Intent i = new Intent(this,LoginActivity.class);
//        startActivity(i);
//    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return id == R.id.action_settings || super.onOptionsItemSelected(item);

    }


    @Override
    public void onListFragmentInteraction(Evento item) {
        Toast.makeText(this,"Evento?->"+item.getNombre(),Toast.LENGTH_SHORT).show();

    }

    @Override
    public void ListenerMisEventos() {
        Toast.makeText(this,"Abriendo los eventos que has creado",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void CerrarSesion() {
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.d("SIN USUARIO","NO HAY USUARIO");
                        actualizarInterfaz();
                        finish();
                    }
                });
    }

//    Este metodo se encarga de volver a pintar el fragmento de login en donde se necesite
    private void actualizarInterfaz() {
        if(mSectionsPagerAdapter != null)
            mSectionsPagerAdapter.notifyDataSetChanged();
    }


    //    Agregamos el listener a nuestra activity
    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(authStateListener);
    }

//    Quitamos el listener a nuestra activity
    @Override
    protected void onStop() {
        super.onStop();
        if(authStateListener!=null)
            firebaseAuth.removeAuthStateListener(authStateListener);
    }

    @Override
    public void Login() {
        Log.d("LOGIN","Desde el main Activity");
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(
                                Arrays.asList(
                                        new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build()))
                        .build(),
                SIGN_IN_CODE);
    }

    @Override
    public void Tocar(View view) {
        reproducirSonido(Integer.parseInt(view.getTag().toString()));

    }

    private void reproducirSonido(int i) {
//        Buscas en el arreglo el sonido deseado
        Log.d("boton","REPRODUCIENDO "+i);
    }


    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
           if(FirebaseAuth.getInstance().getCurrentUser() != null){
               if(position ==0){
                   return EventoFragment.newInstance();
               }
               else {
                   return Perfil.newInstance();
               }
           }else{
               return NoUser.newInstance();
           }

        }

        @Override
        public int getCount() {

            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Eventos";
                case 1:
                    return "Perfil";

            }
            return null;
        }
    }

    private void showSnackbar(String errorMessageRes) {
        Snackbar.make(fab, errorMessageRes, Snackbar.LENGTH_LONG).show();
    }

}
