package dev.android.alexche77.calendariofamiliar;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by alvaro on 9/23/17.
 */

public class MisPortadasAdapter extends RecyclerView.Adapter<MisPortadasAdapter.ViewHolder> {

    private final ArrayList<StorageReference> referencias;
    private final Context context;
    private ListenerPortada mClickListener;
    public MisPortadasAdapter(Context context, ArrayList<StorageReference> referencias){
        this.context = context;
        this.referencias = referencias;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_portada, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Log.d("VIEWHOLDER","OBTENIENDO->"+position+" de una lista que tiene"+referencias.size());
        try {
            final File tmp = File.createTempFile("imagenes","jpg");
            referencias.get(position).getFile(tmp).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    Log.d("ARCHIVO", "onSuccess: "+tmp.toString());
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

        referencias.get(position).getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                Log.d("PORTADAS","Obtenido->"+task.getResult());
                holder.portadaImg.setVideoURI(task.getResult());
                holder.portadaImg.start();
//                Glide.with(context)
//                        .load(task.getResult())
//                        .into(holder.portadaImg);
            }
        });


    }

    @Override
    public int getItemCount() {
        return referencias.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{
        VideoView portadaImg;
        public ViewHolder(View itemView) {
            super(itemView);
            portadaImg = itemView.findViewById(R.id.portadaItem);
            portadaImg.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(referencias.get(getAdapterPosition()));
        }
    }

//    Nos permite implementar listener de clics
public void setClickListener(ListenerPortada itemClickListener) {
        this.mClickListener = itemClickListener;
    }


    public interface ListenerPortada {
        void onItemClick(StorageReference reference);
    }

}
