package dev.android.alexche77.calendariofamiliar.Fragmentos.POJO;

/**
 * Created by alvaro on 9/15/17.
 */

public class Invitados {

//    En este POJO no necesitamos ID, cada nodo genera su propio ID.
//    Al finalizar la creacion del nodo, se envia el ID al evento correspondiente.

    private String idUser;
    private String nombreUser;
    private int estadoInvitacion;
    private long fecha_invitacion;
    private long fecha_respondido;

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getNombreUser() {
        return nombreUser;
    }

    public void setNombreUser(String nombreUser) {
        this.nombreUser = nombreUser;
    }

    public int getEstadoInvitacion() {
        return estadoInvitacion;
    }

    public void setEstadoInvitacion(int estadoInvitacion) {
        this.estadoInvitacion = estadoInvitacion;
    }

    public long getFecha_invitacion() {
        return fecha_invitacion;
    }

    public void setFecha_invitacion(long fecha_invitacion) {
        this.fecha_invitacion = fecha_invitacion;
    }

    public long getFecha_respondido() {
        return fecha_respondido;
    }

    public void setFecha_respondido(long fecha_respondido) {
        this.fecha_respondido = fecha_respondido;
    }
}
