package dev.android.alexche77.calendariofamiliar.Fragmentos.POJO;

/**
 * Created by alvaro on 9/15/17.
 */

public class Posts {

/*Esta entidad guarda los posts de cada evento.
* Estan ordenados respecto a su ID, segun su creacion, por lo tanto
* el ID menor es el primer post y el ID mayor es el mas reciente.
* Esto es conveniente al momento de traerlos.
*
* El ID no es autogenerado, pero eso se contempla al momento de hacer un post dentro del ejemplo.
* El nodo del post tendra como clave el id generado.
*
* */

    private String idUsuario;
    private long fecha_post;        //Timestamp autogenerada por firebase
    private String texto;           //Texto que contenga el post. Puede ser nulo.
    private boolean tieneFoto;      //Si el post tiene fotos, actualmente solo se contempla una foto
                                    //por post, la logica es la misma, si el post tiene foto entonces
                                    //se busca en la carpeta eventos/posts/<id_post>.png
                                    //Y se trae.

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public long getFecha_post() {
        return fecha_post;
    }

    public void setFecha_post(long fecha_post) {
        this.fecha_post = fecha_post;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public boolean isTieneFoto() {
        return tieneFoto;
    }

    public void setTieneFoto(boolean tieneFoto) {
        this.tieneFoto = tieneFoto;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    private int likes;              //Por el momento dejemoslo en Int, :v

}
