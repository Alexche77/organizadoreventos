package dev.android.alexche77.calendariofamiliar;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;

import com.google.firebase.storage.StorageReference;

import dev.android.alexche77.calendariofamiliar.Fragmentos.DialogFragmentPortadas;

public class CrearEvento extends AppCompatActivity  implements DatePickerDialog.OnDateSetListener{

    private int anho,mes,dia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_evento);

    }


    public void mostrarPickerPortadas(View v){

        DialogFragmentPortadas dPortadas = new DialogFragmentPortadas();
        dPortadas.show(getSupportFragmentManager(),"PICKER_PORTADAS");

    }

    public void mostrarDatePicker(View v){
        DatePickerDialog dp = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            dp = new DatePickerDialog(this);
            dp.setOnDateSetListener(this);
        }

        if (dp != null)
            dp.show();

    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        dia = i2;
        mes = i1;
        anho = i;
    }


    public void referenciaSeleccionada(StorageReference ref) {
        Log.d("REFERENCIA","Obtenida la referencia->"+ref.getName());
    }


}
