package dev.android.alexche77.calendariofamiliar.Fragmentos;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import dev.android.alexche77.calendariofamiliar.Fragmentos.POJO.Evento;
import dev.android.alexche77.calendariofamiliar.R;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class EventoFragment extends Fragment {
    private ArrayList<Evento> eventos;
    private OnListFragmentInteractionListener mListener;
    private MyEventoRecyclerViewAdapter mAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public EventoFragment() {
    }

    public static EventoFragment newInstance() {
        return new EventoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_evento_list, container, false);
        traerEventos();
        mAdapter = new MyEventoRecyclerViewAdapter(eventos, mListener);
        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(mAdapter);
        }
        return view;
    }


    /**
     * En este metodo se traen desde firebase los eventos en los que ha sido invitado el usuario
     * que esta actualmente loggeado la servidor.
     * Se debe retornar de manera ordenada descendientemente a partir de la fecha.
     * Los eventos que ya pasaron no se retornaran desde el servidor,estos son
     * accesibles unicamente desde la pantalla donde se muestra el historial de los eventos donde
     * el usuario ha ido.
     */
    private void traerEventos() {
        eventos = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            eventos.add(new Evento("ID_"+i,"Evento#"+i,"ID_DETALLE:"+i,i,i,i,i));
        }
        if(mAdapter != null)
            mAdapter.notifyDataSetChanged();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Evento item);
    }
}
